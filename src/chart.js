let lineCtx = document.getElementById("lineChart").getContext("2d");
let pieCtx = document.getElementById("pieChart").getContext("2d");
let polarCtx = document.getElementById("polarChart").getContext("2d");

let randomSalesData = Array.from({ length: 12 }, () =>
  Math.floor(Math.random() * 1000)
);
let randomSalesData2 = Array.from({ length: 12 }, () =>
  Math.floor(Math.random() * 1000)
);

function generateRandomPieData(numCategories) {
  const randomData = [];
  for (let i = 0; i < numCategories; i++) {
    randomData.push(Math.floor(Math.random() * 1000));
  }
  return randomData;
}

//Line Chart

let lineChart = new Chart(lineCtx, {
  type: "line",
  data: {
    labels: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ],
    datasets: [
      {
        label: "Sales",
        data: randomSalesData,
        borderColor: "rgba(75, 192, 192, 1)",
        backgroundColor: "rgba(75, 192, 192, 0.2)",
        fill: true,
        pointStyle: "circle",
        pointRadius: 3,
        pointHoverRadius: 15,
      },
      {
        label: "Last year sales",
        data: randomSalesData2,
        borderColor: "rgba(255, 99, 132, 1)",
        backgroundColor: "rgba(255, 99, 132, 0.2)",
        fill: true,
        pointStyle: "circle",
        pointRadius: 3,
        pointHoverRadius: 15,
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    tension: 0.15,
    interaction: {
      mode: "index",
      intersect: false,
    },
    stacked: false,
    plugins: {
      title: {
        display: false,
      },
      legend: {
        display: false,
      },
    },
    scales: {
      y: {
        type: "linear",
        display: true,
        position: "left",
      },
      y1: {
        type: "linear",
        display: true,
        position: "right",

        grid: {
          drawOnChartArea: false,
        },
      },
    },
  },
});

//PieChart

const numCategories = 4;
const randomPieData = generateRandomPieData(numCategories);

let pieChart = new Chart(pieCtx, {
  type: "doughnut",
  data: {
    labels: ["Groceries", "Snacks", "Beverages", "Dairy"],
    datasets: [
      {
        data: randomPieData,
        backgroundColor: [
          "rgba(255, 99, 132, 0.6)",
          "rgba(255, 205, 86, 0.6)",
          "rgba(75, 192, 192, 0.6)",
          "rgba(54, 162, 235, 0.6)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(255, 205, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(54, 162, 235, 1)",
        ],
      },
    ],
  },
  options: {
    title: {
      display: true,
      text: "Monthly Product Sales",
    },
  },
});

//Polar Chart

let data = {
  labels: ["Shipped", "In Progress", "Pending"],
  datasets: [
    {
      data: [4, 3, 3],
      backgroundColor: [
        "rgba(75, 192, 192, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(255, 99, 132, 1)",
      ],
    },
  ],
};

let polarChart = new Chart(polarCtx, {
  type: "polarArea",
  data: data,
  options: {
    responsive: false,
    labels: false,
    scales: {
      r: {
        pointLabels: {
          display: true,
          centerPointLabels: true,
          font: {
            size: 18,
          },
        },
      },
    },
    plugins: {
      legend: {
        position: "top",
      },
    },
  },
});
